package AbstractFactory.demoprogram;

public interface Shape {
    void draw();
}