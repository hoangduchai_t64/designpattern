package AbstractFactory.demoprogram;

public abstract class AbstractFactory {
    abstract Shape getShape(String shapeType);
}