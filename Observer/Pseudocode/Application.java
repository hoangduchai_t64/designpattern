package Observer.Pseudocode;

import java.io.IOException;

public class Application {
    public static void main(String[] args) throws IOException {
        Editor editor = new Editor();
        LoggingListener loggingListener = new LoggingListener(
                "log.txt",
                "Someone has opened the file: %s");
        EmailAlertsListener emailAlertsListener = new EmailAlertsListener("email.txt",
                "Someone has changed the file: %s");

        editor.events.subscribe("open",loggingListener);
        editor.events.subscribe("save",emailAlertsListener);

        editor.openFile("test.txt");
        editor.saveFile();

    }
}
