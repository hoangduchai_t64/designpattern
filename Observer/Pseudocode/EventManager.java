package Observer.Pseudocode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventManager {
    private Map<String, List<EventListeners>> listeners = new HashMap<>();

    public void subscribe(String eventType, EventListeners evenManager){
        if(listeners.get(eventType) == null){
          listeners.put(eventType,new ArrayList<>());
        }
        if(evenManager != null) {
            listeners.get(eventType).add(evenManager);
        }
    }
    public void unSubscribe(String eventType, EventListeners evenManager){
        if(evenManager != null && listeners.get(eventType) != null){
            listeners.get(eventType).remove(evenManager);
        }
    }

    public void notify(String eventType,String data){
        for (EventListeners listener: listeners.get(eventType)) {
            listener.update(data);
        }
    }
}
