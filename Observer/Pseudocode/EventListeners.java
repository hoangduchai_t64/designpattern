package Observer.Pseudocode;

public interface EventListeners {
    public void update(String filename);
}
