package Observer.Pseudocode;

import java.io.File;
import java.io.IOException;

public class Editor {
    public EventManager events;
    private File file;

    public Editor(){
        events = new EventManager();
    }
    public void openFile(String path){
        this.file = new File(path);
        events.notify("open",file.getName());
    }
    public void saveFile() throws IOException {
        if(file != null){
            events.notify("save",file.getName());
        }else {
            throw new IOException("Please open the file first.");
        }
    }
}
