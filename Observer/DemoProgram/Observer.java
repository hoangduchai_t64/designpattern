package Observer.DemoProgram;

public abstract class Observer {
    public Subject subject;

    public abstract void update();
}
