package FactoryMethod.DemoProgram;

public interface Fruit {
    public void produceJuice();
}
