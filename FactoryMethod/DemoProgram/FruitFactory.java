package FactoryMethod.DemoProgram;

public class FruitFactory {
    public Fruit provideFruit(String type) throws Exception {
        switch (type){
            case "Apple" : return new Apple();
            case "Banana" : return new Banana();
            case "Orange" : return new Orange();
        }
        throw new Exception("This product was not found");
    }
}
