package FactoryMethod.DemoProgram;

public class Orange implements Fruit{
    @Override
    public void produceJuice() {
        System.out.println("this is Orange Juice");
    }
}
