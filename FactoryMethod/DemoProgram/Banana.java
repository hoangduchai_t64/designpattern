package FactoryMethod.DemoProgram;

public class Banana implements Fruit{
    @Override
    public void produceJuice() {
        System.out.println("this is Banana Juice");
    }
}
