package FactoryMethod.DemoProgram;

public class CallingClass {
    public static void main(String[] args) throws Exception {
        FruitFactory factory = new FruitFactory();

        Fruit fruit = factory.provideFruit("Apple");
        fruit.produceJuice();

        fruit = factory.provideFruit("Orange");
        fruit.produceJuice();

        fruit = factory.provideFruit("Banana");
        fruit.produceJuice();

    }
}
