package FactoryMethod.Pseudocode;

public class Application {
    private static Dialog dialog;
    public static void initialize(String type) throws Exception {
        if(type.equals("Windows")){
            dialog = new WindowsDialog();
        }else if(type.equals("Web")){
            dialog = new WebDialog();
        }
        else {
            throw new Exception("Error! Unknown operating system.");
        }
    }
    public static void main(String[] args) throws Exception {
        initialize("Windows");
        dialog.render();
    }
}
